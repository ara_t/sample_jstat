const path = require("path");

module.exports = {
    mode: "production", // "production" | "development" | "none"

    // entrypoint
    entry: path.join(__dirname, "src/main.ts"),

    output: {
        path: path.join(__dirname, "dist"),
        filename: "main.js",
        publicPath: "/dist/"
    },

    devServer: {
        static: {
            directory: path.join(__dirname, "/"),
            watch: true,
        },
        // hot: "only",
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "ts-loader"
            }
        ]
    },

    resolve: {
        modules: [
            "node_modules",
        ],
        extensions: [
            ".ts", ".js"
        ]
    }
};

