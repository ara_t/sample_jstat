import jstat from "jstat";
const {
    normal,
    studentt,
    chisquare,
    binomial,
    poisson
} = jstat;

console.log("========== normal ==========");
console.log("normal.pdf(1.5, 0, 1) =", normal.pdf(1.5, 0, 1));
console.log("normal.cdf(1.5, 0, 1) =", normal.cdf(1.5, 0, 1));
console.log("normal.inv(0.7, 0, 1) =", normal.inv(0.7, 0, 1));

console.log("\n========== studentt ==========");
console.log("studentt.pdf(2, 3.5) =", studentt.pdf(2, 3.5));
console.log("studentt.cdf(2, 3.5) =", studentt.cdf(2, 3.5));
console.log("studentt.inv(0.7, 3.5) =", studentt.inv(0.7, 3.5));

console.log("\n========== chisquare ==========");
console.log("chisquare.pdf(2, 2.5) =", chisquare.pdf(2, 2.5));
console.log("chisquare.cdf(2, 2.5) =", chisquare.cdf(2, 2.5));
console.log("chisquare.inv(0.95, 2.5) =", chisquare.inv(0.95, 2.5));

console.log("\n========== binomial ==========");
console.log("binomial.pdf(3, 10, 0.6) =", binomial.pdf(3, 10, 0.6));
console.log("binomial.cdf(3, 10, 0.6) =", binomial.cdf(3, 10, 0.6));

console.log("\n========== poissin ==========");
console.log("poisson.pdf(1, 2) =", poisson.pdf(1, 2));
console.log("poisson.cdf(1, 2) =", poisson.cdf(1, 2));

